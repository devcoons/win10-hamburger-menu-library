﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BurgerMenu
{

    public delegate void Action();

    public sealed partial class UCMenu : UserControl
    {

        public int selectedItem = 0;

        public SplitView sview;
        public UCMenu()
        {
            this.InitializeComponent();
        }

   
        public void addMenuItem(UCMenuItem item, Action action)
        {
            item.action = action;
            myStackPanel.Children.Add(item);         
            myStackPanel.UpdateLayout();
        }


        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            sview.IsPaneOpen = !sview.IsPaneOpen;
        }
    }
}
