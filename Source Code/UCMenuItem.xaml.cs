﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BurgerMenu
{
    public sealed partial class UCMenuItem : UserControl
    {
        public int selected = 0;
        public Color onMouseEnterBackground = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
        public Color onMouseLeaveBackground = Color.FromArgb(0x00, 0xFF, 0xFF, 0xFF);
        public Color onMouseClickBackground = Color.FromArgb(0xFF, 0xAF, 0xAF, 0xAF);
        public Action action;

        public int Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        public static readonly DependencyProperty SelectedContent =
           DependencyProperty.Register("Selected", typeof(int), typeof(UCMenuItem), null);



        public String ItemIcon
        {
            get { return (string)MenuButton1.Content; }
            set { SetValue(MenuButtonContent, value); }
        }

        public static readonly DependencyProperty MenuButtonContent =
           DependencyProperty.Register("ItemIcon", typeof(string), typeof(UCMenuItem), null);

        public String ItemText
        {
            get { return (string)textBlock1.Content; }
            set { SetValue(TextBlockText, value); }
        }

        public static readonly DependencyProperty TextBlockText =
           DependencyProperty.Register("ItemText", typeof(string), typeof(UCMenuItem), null);

        public UCMenuItem(string icon,string text)
        {
            this.InitializeComponent();
            MenuButton1.Content = icon;
            textBlock1.Content = text;
        }

        private void UserControl_Tapped(object sender, TappedRoutedEventArgs e)
        {
           
        }

        private void UserControl_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            if (selected != 1)
            {
                spanel.Background = new SolidColorBrush(onMouseEnterBackground);
            }
            else
            {
                spanel.Background = new SolidColorBrush(onMouseEnterBackground);
            }
            if (action != null)
            {
                action();
            }
         }

        private void UserControl_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            if (selected != 1)
            {
                spanel.Background = new SolidColorBrush(onMouseEnterBackground);
            }
            else
            {
                spanel.Background = new SolidColorBrush(onMouseEnterBackground);
            }
        }

        private void UserControl_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            if (selected != 1)
            {
                spanel.Background = new SolidColorBrush(onMouseLeaveBackground);
            }
            else
            {
                spanel.Background = new SolidColorBrush(onMouseEnterBackground);
            }
          
        }

        private void UserControl_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            spanel.Background = new SolidColorBrush(onMouseClickBackground);
        }

        private void spanel_Loaded(object sender, RoutedEventArgs e)
        {
            if (selected == 1)
            {
                spanel.Background = new SolidColorBrush(onMouseEnterBackground);
            }
        }
    }
}
